/**
 * This file is part of the ExaHyPE project.
 * Copyright (c) 2020  http://exahype.eu
 * All rights reserved.
 *
 * The project has received funding from the European Union's Horizon
 * 2020 research and innovation programme under grant agreement
 * No 671698. For copyrights and licensing, please consult the webpage.
 *
 * Released under the BSD 3 Open Source License.
 * For the full license text, see LICENSE.txt
 **/


#ifndef EXAHYPE_OFFLOADING_SOFTERRORINJECTOR_H_
#define EXAHYPE_OFFLOADING_SOFTERRORINJECTOR_H_

#include <atomic>
#include <random>

#include "tarch/logging/Log.h"
#include "tarch/la/Vector.h"

#include "exahype/solvers/Solver.h"

namespace exahype {
  namespace reactive {
    class ResilienceTools;
  }
}

/**
 * Utility class for ExaHyPE's reactive resilience mechanisms (error detection and correction).
 * Contains functions to inject errors according to specified parameters such as injection position (spatial), frequency and error value.
 * It also allows to check double arrays for equality for error detection.
 */
class exahype::reactive::ResilienceTools {
  public:
  
  /**
   * Used to represent the error generation strategy.
   * None: No error should be injected.
   * Bitflips: A random bit at random location should be flipped.
   * OverwriteRandomValueWithFixedError: Chooses a random value in an array and overwrites it with a fixed error.
   * OverwriteRandomValueWithRandomError: Chooses a random value in an array and overwrites it with a random error.
   * OverwriteHardcoded: Overwrites array at a hardcoded index (currently 0) with a fixed error. Here, an injection position (spatial coordinates of the cell) needs to be specified.
   *                     Further, the time stamp for injection (e.g., inject at t=0.01s) and/or a fixed injection frequency (inject every k time steps) has to be specified.
   */
  enum class SoftErrorGenerationStrategy { None, Bitflips, OverwriteRandomValueWithFixedError, OverwriteHardcoded, OverwriteRandomValueWithRandomError };

  private:

  /**
   * The log device of this class.
   */
  static tarch::logging::Log _log;

  /**
   * Controls how many errors should be injected in an application run.
   */
  int _maxNumInjections;

  /**
   * Internal counter of executed STPs since last error injection.
   */
  std::atomic<int> _cntSinceLastInjection;

  /**
   * Counts how many errors have already been injected in this run.
   */
  std::atomic<int> _numInjected;

  /**
   * @brief Indicates that the current state is corrupted. This flag is 
   * presently only used for reporting purposes (is the solution corrupted?)
   * and should not be used for decision making.
   */
  bool _corruptionDetected;

  /**
   * @brief Rank on which an error should be injected for all 
   * error injection strategies except for the hardcoded one. 
   * Currently, errors are only injected on team 0.
   */
  int _injectionRank;

  /**
   * @brief The error value for the error injection strategies which hardcode an error.
   */
  double _injectedErrorVal;

  /**
   * Specifies after how many STPs a soft-error is injected.
   */
  int _injectionFrequency;

  /**
   * Specifies the position of the cell centre into which 
   * an error should be injected (hardcoded strategy).
   */
  tarch::la::Vector<DIMENSIONS, double> _injectionPosition;

  /**
   * Specifies a time stamp for the time step at which an error should be injected (hardcoded strategy).
   */
  double _injectionTime;

  /**
   * The tolerance threshold for the derivatives error 
   * criterion above which a task outcome is considered dubious.
   */
  double _maxErrorCriterionDerivatives;

  /**
   * The tolerance threshold for the time step sizes error 
   * criterion above which a task outcome is considered dubious.
   */
  double _maxErrorCriterionTimeStepSizes;

  public:

  /**
   * @brief Stores the selected error injection strategy.
   */
  static SoftErrorGenerationStrategy GenerationStrategy;

  /**
   * @brief Indicates whether all migratable STP tasks should be checked.
   * 
   */
  static bool CheckAllMigratableSTPs;

  /**
   * @brief Indicates whether only the migratable STP tasks
   * with a low confidence (i.e., a high error indicator) should be checked.
   */
  static bool CheckSTPsWithLowConfidence;

  /**
   * @brief Indicates whether only migratable STP tasks for cells 
   * in which the limiter was activated (in the correction step) should be checked.
   */
  static bool CheckLimitedCellsOnly;

  /**
   * @brief Indicates whether only corrupted STP tasks should be checked (for debugging and developement).
   */
  static bool CheckFlipped;

  /**
   * @brief Indicates if the derivatives criterion should be activated.
   */
  static bool CheckSTPDerivatives;

  /**
   * @brief Indicates if the time step sizes criterion should be activated.
   */
  static bool CheckSTPTimeSteps;

  /**
   * @brief Indicates if the admissibility checks criterion should be activated.
   */
  static bool CheckSTPAdmissibility;

  /**
   * @brief Indicates if criteria are to be evaluated lazily: cheap criteria act as a pre-filter 
   * before the more expensive derivatives criterion is evaluated.
   */
  static bool CheckSTPsLazily;

  static void setSoftErrorGenerationStrategy(SoftErrorGenerationStrategy strat);

  // absolute error norms
  static double computeInfNormError(const double *a1, const double *a2, size_t length);
  static double computeL1NormError(const double *a1, const double *a2, size_t length);
  static double computeL2NormError(const double *a1, const double *a2, size_t length);

  // relative error norms
  static double computeInfNormErrorRel(const double *a1, const double *a2, size_t length);
  static double computeL1NormErrorRel(const double *a1, const double *a2, size_t length);
  static double computeL2NormErrorRel(const double *a1, const double *a2, size_t length);

  static ResilienceTools& getInstance();

  /**
   * @brief Configures error injection. This should be called at program startup.
   * 
   * @param injectedError The hardcoded error value (for error strategies with hardcoded error).
   * @param injectionTime The time stamp of the time step at which the error should be injected (hardcoded error injection strategy).
   * @param injectionPos  The position at which the error should be injected (cell center coordinates, for hardcoded error injection strategy).
   * @param injectionRank The rank on which an error should be injected (for strategies different from hardcoded error injection where injection position is irrelevant).
   * @param injectionFrequency 
   * @param maxNumInjections 
   * @param maxErrorCriterionDerivatives 
   * @param maxErrorCriterionTimeStepSizes 
   */
  void configure(double injectedError,
                 double injectionTime,
                 tarch::la::Vector<DIMENSIONS, double> injectionPos,
                 int injectionRank,
                 int injectionFrequency,
                 int maxNumInjections,
                 double maxErrorCriterionDerivatives,
                 double maxErrorCriterionTimeStepSizes);

  /**
   * @brief Corrupts data (i.e., injects an error) in a given array according to the selected error injection strategy.
   *       To be used as follows: First, check if an error should be injected by calling shouldInjectError. 
   *       If true, the corruptData function should be called next.
   * 
   *       Note: errors are injected in update arrays "array" (update from STP), which are applied to a reference array (e.g., solution) "ref" as:
   *                ref_new[:] = ref[:] + array[:] (e.g., for the correction in ADER-DG). 
   *           
   * @param ref Reference array to which update in array is applied.
   * @param center Position of the center of the cell into which an error should be injected.
   * @param dim Dimension of the grid (=size of center).
   * @param t Current time stamp.
   * @param array Update which is corrupted.
   * @param length Length of arrays ref and array.
   */
  void corruptData(const double *ref, double *center, int dim, double t, double *array, size_t length);

  /**
   * @brief Decides if an outcome - with the computed error criterion input values - is considered as trustworthy
   *  (i.e. as unaffected by a silent error).
   * 
   * @param errorCriterionDerivatives Value of the derivates criterion.
   * @param errorCriterionTimeStepSizes  Value of the time step sizes criterion.
   * @param errorCriterionAdmissibility  Value of the admissibility criterion.
   * @return true if the outcome is trustworthy.
   */
  bool isTrustworthy(double errorCriterionDerivatives, 
                    double errorCriterionTimeStepSizes,
                    double errorCriterionAdmissibility) const;

  bool violatesAdmissibility(double errorCriterion) const;
  bool violatesDerivatives(double errorCriterion) const;
  bool violatesTimestep(double errorCriterion) const;

  /**
  * @brief Returns true if it is time to inject an error for a 
  * specific cell and time stamp.
  * 
  * @param cellCenter Array of size DIMENSIONS containing the cell's center position coordinates.
  * @param timeStamp Timestamp at which the simulation is for the cell.
  * @return true If it is time to inject an error.
  * @return false If it is not yet time to inject an error.
  */
  bool shouldInjectError(const double *cellCenter, double timeStamp);

  /**
   * @brief Checks two input arrays of equal size for equality.
   * 
   * @param a1 First input array.
   * @param a2 Second input array.
   * @param length Size of both arrays.
   * @return true 
   * @return false 
   */
  bool isEqual(const double *a1, const double *a2, size_t length);

  void setCorruptionDetected(bool corrupted);
  bool getCorruptionDetected();

  private:
  ResilienceTools();

  /**
   * @brief Checks if an error criterion is violated.
   * @return True if the error criterion is violated.
   * 
   *  @param The computed error criterion value for the task outcome.
   *  @param The tolerance threshold above the error criterion is considered to be violated.
   */
  bool violatesCriterion(double errorCriterion, double threshold) const;

  /**
   * @brief Corrupts data by creating a bitflip at a random position in the array "array".
   *        
   * @param ref Reference array to which update in array is applied.
   * @param center Position of the center of the cell into which an error should be injected.
   * @param dim Dimension of the grid (=size of center).
   * @param t Current time stamp.
   * @param array Update which is corrupted.
   * @param length Length of arrays ref and array.
   */
  void generateBitflipErrorInArray(const double *ref, 
                              double *center, int dim,
                              double t, double *array,
                              size_t length);
  
   /**
   * @brief Corrupts data by overwriting one random double value with _injectedErrorVal in the array "array".
   *        
   * @param ref Reference array to which update in array is applied.
   * @param center Position of the center of the cell into which an error should be injected.
   * @param dim Dimension of the grid (=size of center).
   * @param t Current time stamp.
   * @param array Update which is corrupted.
   * @param length Length of arrays ref and array.
   */
  void overwriteRandomValueInArrayWithGivenError(const double *ref,
                              double *center, int dim,
                              double t, double *array, size_t size);
  
   /**
   * @brief Corrupts data by overwriting one random double value with random error in the array "array".
   *        
   * @param ref Reference array to which update in array is applied.
   * @param center Position of the center of the cell into which an error should be injected.
   * @param dim Dimension of the grid (=size of center).
   * @param t Current time stamp.
   * @param array Update which is corrupted.
   * @param length Length of arrays ref and array.
   */
  void overwriteRandomValueInArrayWithRandomError(const double *ref,
                              double *center, int dim, double t,
                              double *array, size_t size);
  
    
   /**
   * @brief Corrupts data by injecting an error at (hardcoded) index 0 in the array "array".
   *        
   * @param ref Reference array to which update in array is applied.
   * @param center Position of the center of the cell into which an error should be injected.
   * @param dim Dimension of the grid (=size of center).
   * @param t Current time stamp.
   * @param array Update which is corrupted.
   * @param length Length of arrays ref and array.
   */
  void overwriteHardcoded(const double *ref, 
                          double *center, int dim, double t,
                          double *array, size_t size);

};
#endif /* EXAHYPE_OFFLOADING_SOFTERRORINJECTOR_H_ */
